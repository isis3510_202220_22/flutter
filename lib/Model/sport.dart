class Sport {
  final int id;
  final String name;
  final String imagePath;
  const Sport({required this.id, required this.name, required this.imagePath});
}