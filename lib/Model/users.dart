import 'package:cloud_firestore/cloud_firestore.dart';

class Users {
  String? referenceId;
  final num cantFriends;
  final num cantMatches;
  final num cantMatchesFavSport;
  final String name;
  final String email;
  final String favSport;
  final String username;
  final String pictureLocation;

  Users({
    this.referenceId,
    required this.cantFriends,
    required this.cantMatches,
    required this.cantMatchesFavSport,
    required this.name,
    required this.email,
    required this.username,
    required this.pictureLocation,
    required this.favSport
  });

  factory Users.fromSnapshot(DocumentSnapshot snapshot) {
    final newUser = Users.fromJson(snapshot.data() as Map<String, dynamic>);

    return newUser;
  }

  factory Users.fromJson(Map<String, dynamic> json) => _userFromJson(json);

  Map<String, dynamic> toJson() => _userToJson(this);

  @override
  String toString() {
    return '';
  }
}



Users _userFromJson(Map<String, dynamic> json) {

  return Users(
      cantFriends: json['cantFriends'] as num,
      cantMatches: json['cantMatches'] as num,
      cantMatchesFavSport: json['cantMatchesFavSport'] as num,
      name: json['name'] as String,
      email: json['email'] as String,
      username: json['username'] as String,
      pictureLocation: json['pictureLocation'] as String,
      favSport: json['favSport'] as String

  );
}
Map<String, dynamic> _userToJson(Users instance) => <String, dynamic>{
  'cantFriends': instance.cantFriends,
  'cantMatches': instance.cantMatches,
  'cantMatchesFavSport': instance.cantMatchesFavSport,
  'name': instance.name,
  'email': instance.email,
  'username': instance.username,
  'pictureLocation': instance.pictureLocation,
  'favSport': instance.favSport,

  //'location': instance.location,
};




