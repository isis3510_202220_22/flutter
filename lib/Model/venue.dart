import 'package:cloud_firestore/cloud_firestore.dart';

class Venue {
  String? referenceId;
  final String name;
  final String img;
  final String description;
  final String address;
  final num rating;
  final double longitude;
  final double latitude;
  final String sport;

  Venue(
      {this.referenceId,
      required this.name,
      required this.img,
      required this.description,
      required this.address,
      required this.rating,
      required this.longitude,
      required this.latitude,
      required this.sport,
      });

  factory Venue.fromSnapshot(DocumentSnapshot snapshot) {
    final newVenue = Venue.fromJson(snapshot.data() as Map<String, dynamic>);
    newVenue.referenceId = snapshot.reference.id;
    return newVenue;
  }

  factory Venue.fromJson(Map<String, dynamic> json) => _venueFromJson(json);

  Map<String, dynamic> toJson() => _venueToJson(this);

  @override
  String toString() {
    return '';
  }
}

Venue _venueFromJson(Map<String, dynamic> json) {

  return Venue(
    name: json['name'] as String,
    img: json['img'] as String,
    description: json['description'] as String,
    address: json['address'] as String,
    rating: json['rating'] as num,
    longitude: json['location'].longitude as double,
    latitude: json['location'].latitude as double,
    sport: json['sport'] as String,
  );
}

Map<String, dynamic> _venueToJson(Venue instance) => <String, dynamic>{
      'name': instance.name,
      'img': instance.img,
      'description': instance.description,
      'address': instance.address,
      'rating': instance.rating,
      //'location': instance.location,
      'sport': instance.sport
    };
