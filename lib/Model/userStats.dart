class UserStats {
  final String friends;
  final String matches;

  const UserStats(this.friends, this.matches);
}
