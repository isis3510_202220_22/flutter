class User {
  final String name;
  final String username;
  final String imagePath;
  final String favSport;
  const User({required this.name, required this.username, required this.imagePath, required this.favSport});
}
