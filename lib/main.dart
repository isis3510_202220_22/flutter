import 'dart:async';
import 'package:flutter/services.dart';
import 'package:athletim/Components/bottom_nav_bar.dart';
import 'package:athletim/ViewModel/view_model.dart';
import 'package:athletim/bloc/bloc/auth_bloc.dart';
import 'package:athletim/screens/sign_in.dart';
import 'package:athletim/services/authentication_service.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  runZonedGuarded<Future<void>>(() async {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp();
    // The following lines are the same as previously explained in "Handling uncaught errors"
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
    FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);

    runApp(MyApp());
  }, (error, stack) => FirebaseCrashlytics.instance.recordError(error, stack));
}

class MyApp extends StatelessWidget {
  static FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);

  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ViewModel()),
        Provider<FirebaseAnalytics>.value(value: analytics),
      ],
      child: RepositoryProvider(
        create: (context) => AuthenticationService(),
        child: BlocProvider(
          create: (context) => AuthBloc(
            authRepository:
                RepositoryProvider.of<AuthenticationService>(context),
          ),
          child: MaterialApp(
            navigatorObservers: <NavigatorObserver>[observer],
            debugShowCheckedModeBanner: false,
            title: 'Athletim',
            theme: ThemeData(
              fontFamily: 'Poppins',
              primarySwatch: Colors.blue,
            ),
            home: StreamBuilder<User?>(
                stream: FirebaseAuth.instance.authStateChanges(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return const BottomNavBar();
                  }
                  return SignIn();
                }),
          ),
        ),
      ),
    );
  }
}
