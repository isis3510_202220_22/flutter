import 'package:athletim/Utils/user_preference.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ProfilePicture extends StatelessWidget {
  final actualUser = UserPreference.actualUser;
  final String value;
  const ProfilePicture({Key? key, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("my link is");
    print(value);
    return Container(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.grey,
          border: Border.all(color: Colors.limeAccent.shade400, width: 3),
          shape: BoxShape.circle,
        ),
        height: 125,
        width: 125,
        child: Center(
          child: ClipOval(
            child: CachedNetworkImage(
              imageUrl: value.trim(),
              progressIndicatorBuilder: (context, url, downloadProgress) =>
                  CircularProgressIndicator(value: downloadProgress.progress),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
        ),
      ),
    );
  }
}
