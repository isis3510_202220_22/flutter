import 'package:athletim/Utils/user_preference.dart';
import 'package:athletim/Widgets/profile_pic_widget.dart';
import 'package:flutter/material.dart';

class ProfilePictureEditButton extends StatelessWidget {
  final actualUser = UserPreference.actualUser;
  final String value;
  const ProfilePictureEditButton({Key? key, required this.value})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: [
          ProfilePicture(value: value),
          Positioned(
              bottom: 0,
              right: 4,
              child: GestureDetector(
                onTap: () => print("EDITA PERFIL"),
                child: buildEditIcon(Color(0xFF007163)),
              )),
        ],
      ),
    );
  }

  Widget buildEditIcon(Color color) => buildCircle(
        color: Colors.grey.shade900,
        all: 3,
        child: buildCircle(
          color: color,
          all: 8,
          child: Icon(
            Icons.edit,
            color: Colors.white,
            size: 15,
          ),
        ),
      );
  Widget buildCircle({
    required Widget child,
    required double all,
    required Color color,
  }) =>
      ClipOval(
        child: Container(
          padding: EdgeInsets.all(all),
          color: color,
          child: child,
        ),
      );
}
