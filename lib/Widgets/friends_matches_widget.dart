import 'package:flutter/material.dart';

class FriendsGames extends StatelessWidget {
  final String value;
  final String value2;

  const FriendsGames({Key? key, required this.value, required this.value2})
      : super(key: key);
  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          buildButton(context, value, 'Friends'),
          buildDivider(),
          buildButton(context, value2, 'Matches'),
        ],
      );

  Widget buildDivider() => Container(
        height: 24,
        child: VerticalDivider(
          color: Colors.limeAccent.shade400,
        ),
      );

  Widget buildButton(BuildContext context, String value, String text) =>
      MaterialButton(
        padding: EdgeInsets.symmetric(vertical: 4),
        onPressed: () {},
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              value,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 24,
                  color: Colors.white),
            ),
            SizedBox(height: 2),
            Text(
              text,
              style:
                  TextStyle(fontWeight: FontWeight.normal, color: Colors.white),
            ),
          ],
        ),
      );
}
