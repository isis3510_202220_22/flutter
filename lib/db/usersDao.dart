import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import '../Model/users.dart';

class UsersDao
{
  final CollectionReference collection = FirebaseFirestore.instance.collection('users');

  Stream<QuerySnapshot> getStream() {
    return collection.snapshots();
  }
  Future<List<QueryDocumentSnapshot>> getUsers() async
  {
    print("estoy bajando los usuarios");
    var users = await collection.limit(5).get();
    return users.docs;
  }
  Future<List<QueryDocumentSnapshot>> getNextUsers( DocumentSnapshot beginGet) async
  {
    var nextUsers = await collection.startAfterDocument(beginGet).limit(5).get();
    return nextUsers.docs;
  }
  Future<DocumentReference> addUser( Users user)
  {
    return collection.add(user.toJson());
  }
  void updateVenue(Users user) async{
    await collection.doc(user.referenceId).update(user.toJson());
  }
  void deleteVenue(Users u) async
  {
    await collection.doc(u.referenceId).delete();
  }
}