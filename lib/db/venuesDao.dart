import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import '../Model/venue.dart';

class VenueDao
{
  final CollectionReference collection = FirebaseFirestore.instance.collection('venues');

  Stream<QuerySnapshot> getStream() {
    return collection.snapshots();
  }
  Future<List<QueryDocumentSnapshot>> getVenues() async
  {
    var venues = await collection.limit(5).get();
    return venues.docs;
  }
  Future<List<QueryDocumentSnapshot>> getNextVenues( DocumentSnapshot beginGet) async
  {
    var nextVenues = await collection.startAfterDocument(beginGet).limit(5).get();
    return nextVenues.docs;
  }
  Future<DocumentReference> addVenue( Venue venue)
  {
    return collection.add(venue.toJson());
  }
  void updateVenue(Venue venue) async{
    await collection.doc(venue.referenceId).update(venue.toJson());
  }
  void deleteVenue(Venue v) async
  {
    await collection.doc(v.referenceId).delete();
  }
}