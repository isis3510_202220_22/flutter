import 'package:athletim/Model/venue.dart';
import 'package:athletim/Model/users.dart';
import 'package:athletim/db/venuesDao.dart';
import 'package:athletim/db/usersDao.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:math';
import 'package:flutter/material.dart';

class ViewModelUsers extends ChangeNotifier {
  bool _loading = false;
  bool _error = false;

  List<Users> _usersList = [];

  final UsersDao usersDao = UsersDao();
  List<DocumentSnapshot> documentList = [];

  bool get loading => _loading;

  bool get error => _error;


  List<Users> get usersList => _usersList;

  ViewModel() {
    getUsers();
  }

  setLoading(bool loading) async {
    _loading = loading;
    notifyListeners();
  }

  setError(bool error) async {
    _error = error;
    notifyListeners();
  }

  setVenuesList(List<Venue> venuesList) {

  }
  setUsersList(List<Users> usersList) {
    _usersList = usersList;
  }



  getUsers() async {
    setLoading(true);
    var response = await usersDao.getUsers();
    documentList = response;
    response.forEach((users) {
      _usersList.add(Users.fromJson(users.data() as Map<String, dynamic>));
    });
    setUsersList(_usersList);
    setLoading(false);
  }




  updateUsersList(int index) async {
    setLoading(true);
    setError(false);
    _usersList = [];
    index = index == 0 ? 1 : index;
    try {
      var response = await usersDao.getNextUsers(documentList[index - 1]);
      response.forEach((users) {
        if (!documentList.contains(users)) documentList.add(users);
        _usersList.add(Users.fromJson(users.data() as Map<String, dynamic>));
      });
      setUsersList(_usersList);
      setLoading(false);
    } catch (e) {
      setLoading(false);
      setError(true);
    }
  }


}