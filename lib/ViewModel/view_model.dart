import 'package:athletim/Model/venue.dart';
import 'package:athletim/Model/users.dart';
import 'package:athletim/db/venuesDao.dart';
import 'package:athletim/db/usersDao.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:math';
import 'package:flutter/material.dart';

class ViewModel extends ChangeNotifier {
  bool _loading = false;
  bool _error = false;
  List<Venue> _venuesList = [];
  List<Users> _usersList = [];
  final VenueDao venueDao = VenueDao();
  final UsersDao usersDao = UsersDao();
  List<DocumentSnapshot> documentList = [];

  bool get loading => _loading;

  bool get error => _error;

  List<Venue> get venuesList => _venuesList;
  List<Users> get usersList => _usersList;

  ViewModel() {
    getVenues();
    getUsers();
  }

  setLoading(bool loading) async {
    _loading = loading;
    notifyListeners();
  }

  setError(bool error) async {
    _error = error;
    notifyListeners();
  }

  setVenuesList(List<Venue> venuesList) {
    _venuesList = venuesList;
  }
  setUsersList(List<Users> usersList) {
    _usersList = usersList;
  }


  getVenues() async {
    setLoading(true);
    var response = await venueDao.getVenues();
    documentList = response;
    response.forEach((venue) {
      _venuesList.add(Venue.fromJson(venue.data() as Map<String, dynamic>));
    });
    setVenuesList(_venuesList);
    setLoading(false);
  }
  getUsers() async {
    setLoading(true);
    var response = await usersDao.getUsers();
    documentList = response;
    response.forEach((users) {
      _usersList.add(Users.fromJson(users.data() as Map<String, dynamic>));
    });
    setUsersList(_usersList);
    setLoading(false);
  }


  updateList(int index) async {
    setLoading(true);
    setError(false);
    _venuesList = [];
    index = index == 0 ? 1 : index;
    try {
      var response = await venueDao.getNextVenues(documentList[index - 1]);
      response.forEach((venue) {
        if (!documentList.contains(venue)) documentList.add(venue);
        _venuesList.add(Venue.fromJson(venue.data() as Map<String, dynamic>));
      });
      setVenuesList(_venuesList);
      setLoading(false);
    } catch (e) {
      setLoading(false);
      setError(true);
    }
  }

  updateUsersList(int index) async {
    setLoading(true);
    setError(false);
    _usersList = [];
    index = index == 0 ? 1 : index;
    try {
      var response = await usersDao.getNextUsers(documentList[index - 1]);
      response.forEach((users) {
        if (!documentList.contains(users)) documentList.add(users);
        _usersList.add(Users.fromJson(users.data() as Map<String, dynamic>));
      });
      setUsersList(_usersList);
      setLoading(false);
    } catch (e) {
      setLoading(false);
      setError(true);
    }
  }

  sortList(double latitude, double longitude) {
    _venuesList.sort((a, b) => calcDif(a, latitude, longitude)
        .compareTo(calcDif(b, latitude, longitude)));
  }

  calcDif(Venue v, double lat, double lon) {
    var r = 6371;
    var dLat = deg2rad(v.latitude - lat);
    var dLon = deg2rad(lon - v.longitude);
    var a = sin(dLat / 2) * sin(dLat / 2) +
        cos(deg2rad(v.latitude)) *
            cos(deg2rad(lat)) *
            sin(dLon / 2) *
            sin(dLon / 2);
    var c = 2 * atan2(sqrt(a), sqrt(1 - a));
    var d = r * c;
    return d;
  }

  double deg2rad(deg) {
    return deg * (pi / 180);
  }
}
