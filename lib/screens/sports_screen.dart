import 'package:athletim/Utils/sports.dart';
import 'package:athletim/Utils/user_preference.dart';
import 'package:flutter/material.dart';

class SportScreen extends StatefulWidget {
  const SportScreen({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<SportScreen> {
  @override
  Widget build(BuildContext context) {
    return Home();
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class Data {
  final user = UserPreference.actualUser;
  Map fetched_data = {
    "data": [
      {"id": 111, "name": "Basketball", "img": "assets/images/basketball.jpeg"},
      {"id": 222, "name": "Tennis", "img": "assets/images/tennis.jpg"},
      {"id": 333, "name": "Futbol", "img": "assets/images/hulbo.jpg"}
    ]
  };
  List _data = [];

//function to fetch the data

  Data() {
    _data = fetched_data["data"];
  }

  int getId(int index) {
    return _data[index]["id"];
  }

  String getName(int index) {
    return _data[index]["name"];
  }

  String getImage(int index) {
    return _data[index]["img"];
  }

  int getLength() {
    return _data.length;
  }

  void changeOrder(int a, int b) {
    var f = _data[a];
    _data[a] = _data[b];
    _data[b] = f;
  }

  void getPrefered() {
    for (int i = 0; i < _data.length; i++) {
      if (getName(i).toLowerCase() == user.favSport.toLowerCase()) {
        changeOrder(0, i);
        break;
      }
    }
  }
}

class _HomeState extends State<Home> {
  Data _data = new Data();

  @override
  Widget build(BuildContext context) {
    _data.getPrefered();
    return Scaffold(
      backgroundColor: Colors.grey.shade900,
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          foregroundColor: Colors.limeAccent.shade400,
          elevation: 0),
      body: ListView.builder(
        padding: const EdgeInsets.all(5.5),
        itemCount: _data.getLength(),
        itemBuilder: _itemBuilder,
      ),
    );
  }

  Widget _itemBuilder(BuildContext context, int index) {
    return index == 0 ? sportBoxStar(context, index) : sportBox(context, index);
    //onTap: () => MaterialPageRoute(
    //builder: (context) =>

    //SecondRoute(id: _data.getId(index), name: _data.getName(index))),
  }

  Widget sportBoxStar(BuildContext context, int index) {
    return (Stack(children: [
      FractionallySizedBox(
          widthFactor: 1,
          child: Container(
            height: 150,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                border: Border.all(
                  color: Colors.green,
                  width: 3,
                )),
            child: Card(
              color: Colors.grey.shade900,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                  image: DecorationImage(
                    image: AssetImage(
                      _data.getImage(index),
                    ),
                    fit: BoxFit.fitWidth,
                    alignment: Alignment.topCenter,
                  ),
                ),
                child: Text(
                  "\t${_data.getName(index)}",
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 30,
                    color: Colors.limeAccent.shade400,
                  ),
                ),
              ),
            ),
          )), Positioned(
          right: 20,
          top: 2,
          child: Text("Most played", style: TextStyle(color: Colors.green))
          ),
    ]));
  }

  Widget sportBox(BuildContext context, int index) {
    return (Container(
      height: 150,
      padding: EdgeInsets.all(5),
      child: Card(
        color: Colors.grey.shade900,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
            image: DecorationImage(
              image: AssetImage(
                _data.getImage(index),
              ),
              fit: BoxFit.fitWidth,
              alignment: Alignment.topCenter,
            ),
          ),
          child: Text(
            "\t${_data.getName(index)}",
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 30,
              color: Colors.limeAccent.shade400,
            ),
          ),
        ),
      ),
    ));
  }
}
