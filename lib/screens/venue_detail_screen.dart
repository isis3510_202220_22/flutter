import 'package:athletim/Model/venue.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';
import 'package:cached_network_image/cached_network_image.dart';

class VenueDetail extends StatelessWidget {
  final Venue venue;
  const VenueDetail({Key? key, required this.venue}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff000000),
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          foregroundColor: Colors.limeAccent.shade400,
          title: Text(
            venue.name,
            style: TextStyle(
                fontSize: 28, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          elevation: 0),
      body: Container(
        margin: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            CachedNetworkImage(
              imageUrl: venue.img,
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => Text("Not Able to retrieve image"),
              imageBuilder: (context, imageProvider) => Container(
                margin:const EdgeInsets.only(bottom: 20) ,
                height: 200.0,
                width: 200.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: imageProvider,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(25)),
                ),
              )
            ),
            GFRating(
                margin:const EdgeInsets.only(bottom: 20) ,
                value: double.parse(venue.rating.toString()) % 1 == 0
                    ? double.parse(venue.rating.toString())
                    : double.parse(venue.rating.toString()).floor()  + 0.5,
                showTextForm: false,
                controller: TextEditingController(),
                color: Colors.limeAccent.shade400,
                borderColor: Colors.limeAccent.shade400,
                spacing: 2,
                size: 40,
                onChanged: (double rating) {}),
            Center(
             child:Text(venue.address, style: TextStyle(color:Colors.white))
            )
          ],
        ),
      ),
    );
  }
}
