import 'package:athletim/Utils/user_preference.dart';
import 'package:athletim/Widgets/profile_pic_widget.dart';
import 'package:athletim/screens/profile_page.dart';
import 'package:athletim/screens/sports_screen.dart';
import 'package:athletim/screens/users_screen.dart';
import 'package:athletim/screens/venues_screen.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key}) : super(key: key);
  final actualUser = UserPreference.actualUser;

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
        ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 18));

    return Scaffold(
      backgroundColor: Colors.grey.shade900,
      body: Center(
          child: Container(
        margin: EdgeInsets.only(top: 150),
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Text(
              "Welcome Back " + actualUser.name,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 24,
              ),
            ),
            Text(
              actualUser.username,
              style: TextStyle(
                color: Colors.limeAccent.shade400,
                fontSize: 18,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(25.0),
              child: GestureDetector(
                  onTap: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ProfilePage())),
                  child: Center(
                      child: ProfilePicture(
                          value:
                              "https://athletim.s3.amazonaws.com/ic_profile_foreground.png"))),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: SizedBox(
                width: 180, // <-- Your width
                height: 40,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.grey.shade800),
                  ),
                  onPressed: () {
                    FirebaseAnalytics.instance.logEvent(
                        name: 'Search_for_sport', parameters: {'type': 3});

                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const SportScreen()),
                    );
                  },
                  child: Text('Sports'),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: SizedBox(
                width: 180,
                height: 40,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.grey.shade800),
                  ),
                  onPressed: () {
                    FirebaseAnalytics.instance.logEvent(
                        name: 'search_for_partner', parameters: {'type': 3});

                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const UsersScreen()),
                    );
                  },

                  child: Text('Find Partners'),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: SizedBox(
                width: 180, // <-- Your width
                height: 40,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.grey.shade800),
                  ),
                  onPressed: () {
                    FirebaseAnalytics.instance.logEvent(
                        name: 'Search_for_venue', parameters: {'type': 3});
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => VenueScreen()),
                    );
                  },
                  child: Text('Venues'),
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
