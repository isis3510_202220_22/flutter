import 'package:athletim/Model/venue.dart';
import 'package:athletim/ViewModel/view_model.dart';
import 'package:getwidget/getwidget.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';

 /* class VenueDetail extends StatefulWidget {
  const VenueDetail({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<VenueDetail> {
  @override
  Widget build(BuildContext context) {
    ViewModel vm = context.watch<ViewModel>();
    return Home();
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

final _ratingController = TextEditingController();
double _userRating = 4.5;

@override
void initState() {
  _ratingController.text = '4.5';
}

bool _rate = true;

class _HomeState extends State<Home> {
  Location location = Location();
  var latitudeInit = 0.0;
  var longitudeInit = 0.0;
  late LocationData _currentPosition;

  @override
  Widget build(BuildContext context) {
    ViewModel viewModel = context.watch<ViewModel>();
    //getLoc();
    return Scaffold(
      backgroundColor: Color(0xff000000),
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          foregroundColor: Colors.limeAccent.shade400,
          elevation: 0),
      body: Column(children: [
        Align(
            alignment: Alignment.bottomLeft,
            child: Text("\t\t\t\t Closest venues: ",
                style: TextStyle(color: Colors.white))),
        _ui(viewModel)
      ]),
    );
  }

  _ui(ViewModel vm) {
    if (vm.loading) {
      return Container(
        color: Colors.blue,
      );
    }
    return Expanded(
        child: ListView.separated(
            itemBuilder: (context, index) {
              vm.sortList(latitudeInit, longitudeInit);
              Venue venueModel = vm.venuesList[index];
              return Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Color(0xff181818),
                    border: Border.all(
                      color: Color(0xff181818),
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: GFCard(
                  color: Color(0xff181818),
                  boxFit: BoxFit.fill,
                  border: Border.all(),
                  borderRadius: BorderRadius.circular(20.0),
                  titlePosition: GFPosition.start,
                  image: Image.asset(
                    venueModel.imgMap,
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                  ),
                  showImage: true,
                  title: GFListTile(
                    avatar: GFAvatar(
                      backgroundImage: AssetImage(
                        venueModel.img,
                      ),
                    ),
                    title: Text(
                      venueModel.name,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                    subTitle: Text(
                      venueModel.adress,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.normal,
                          fontSize: 14),
                    ),
                  ),
                  content: Text(
                    venueModel.desc,
                    style: TextStyle(color: Colors.white),
                  ),
                  buttonBar: GFButtonBar(
                    children: <Widget>[
                      GFButton(
                        onPressed: () {
                          setState(() {
                            _rate = true;
                          });
                        },
                        color: Colors.limeAccent.shade400,
                        text: "Rate",
                        textStyle: TextStyle(color: Colors.black, fontSize: 15),
                        shape: GFButtonShape.pills,
                      ),
                      GFButton(
                        onPressed: () {},
                        color: Colors.limeAccent.shade400,
                        textStyle: TextStyle(color: Colors.black, fontSize: 15),
                        text: "View in maps",
                        shape: GFButtonShape.pills,
                      ),
                      GFRating(
                        value: venueModel.rating % 1 == 0
                            ? venueModel.rating
                            : venueModel.rating.floorToDouble() + 0.5,
                        showTextForm: false,
                        controller: _ratingController,
                        color: Colors.limeAccent.shade400,
                        borderColor: Colors.black,
                        spacing: 2,
                        size: 30,
                        suffixIcon: GFButton(
                          type: GFButtonType.transparent,
                          color: Colors.limeAccent.shade400,
                          onPressed: () {
                            _rate = false;
                            setState(() {
                              _userRating =
                                  double.parse(_ratingController.text ?? '0.0');
                            });
                          },
                          child: const Text('Rate'),
                        ),
                        onChanged: (double rating) {},
                      ),
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) => Divider(),
            itemCount: vm.venuesList.length));
  }

  getLoc() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    _currentPosition = await location.getLocation();

    location.onLocationChanged.listen((LocationData currentLocation) {
      setState(() {
        _currentPosition = currentLocation;
        if (latitudeInit != currentLocation.latitude) {
          latitudeInit = currentLocation.latitude as double;
        }
        if (longitudeInit != currentLocation.longitude) {
          longitudeInit = currentLocation.longitude as double;
        }
      });
    });
  }
}
*/