import 'dart:async';

import 'package:athletim/Model/users.dart';
import 'package:athletim/ViewModel/view_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UsersScreen extends StatefulWidget {
  const UsersScreen({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<UsersScreen> {
  @override
  Widget build(BuildContext context) {
    return Home();
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

@override
void initState() {}

class _HomeState extends State<Home> {
  ConnectivityResult _connectionStatus = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;
  int pagingIndex = 1;
  @override
  void initState() {
    super.initState();
    initConnectivity();

    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      return;
    }
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }
    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      _connectionStatus = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    ViewModel viewModel = context.watch<ViewModel>();

    return _connectionStatus == ConnectivityResult.none
        ? noConnection()
        //aqui poner toda la monda nueva aaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaa aaaaaaaaa
        : Scaffold(
            backgroundColor: Color(0xff000000),
            appBar: AppBar(
                backgroundColor: Colors.transparent,
                foregroundColor: Colors.limeAccent.shade400,
                title: Text("users"),
                elevation: 0),
            body: Container(
              margin: EdgeInsets.only(bottom: 15),
              child: Column(children: [
                const Align(
                    alignment: Alignment.bottomLeft,
                    child: Text("\t\t Users",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 25))),
                VenuesPaginatedList(key: UniqueKey(), vm: viewModel),
                Container(
                  margin: const EdgeInsets.only(left: 14, right: 14),
                  padding: const EdgeInsets.only(top: 1, bottom: 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Material(
                          color: Colors.transparent,
                          child: IconButton(
                            icon: Image.asset("assets/icons/leftArrow.png"),
                            onPressed: () => setState(() {
                              if (pagingIndex > 1) {
                                pagingIndex = pagingIndex - 1;
                                viewModel
                                    .updateUsersList((pagingIndex - 1) * 5);
                              }
                            }),
                          )),
                      Text(
                        pagingIndex.toString(),
                        style: TextStyle(
                            color: Colors.limeAccent.shade400,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                      Material(
                          color: Colors.transparent,
                          child: IconButton(
                            icon: Image.asset("assets/icons/rightArrow.png"),
                            onPressed: () => setState(() {
                              pagingIndex = pagingIndex + 1;
                              viewModel.updateUsersList((pagingIndex - 1) * 5);
                            }),
                          )),
                    ],
                  ),
                )
              ]),
            ));
  }

  noConnection() {
    return (Container(
        decoration: BoxDecoration(
            color: Color(0xff2E3330), borderRadius: BorderRadius.circular(15)),
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(left: 15, right: 15, top: 0, bottom: 700),
        height: 30,
        //color: Color(0xff2E3330),

        child: Container(
            child: Row(
          children: [
            Container(
                child: Image.asset(
              "assets/icons/error.png",
              height: 100,
              width: 100,
            )),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Error",
                      style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 18)),
                  Text("no connection",
                      style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.normal,
                          fontSize: 10))
                ],
              ),
            ),
            Material(
                color: const Color(0xff0000),
                child: IconButton(
                  icon: Image.asset('assets/icons/graytriangle.png'),
                  iconSize: 50,
                  color: Color.fromRGBO(84, 84, 84, 90),
                  onPressed: () {
                    /*
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => VenueDetail(venue: venue)),
            );
            */
                  },
                ))
          ],
        ))));
    /*

    return Scaffold(
        backgroundColor: Color(0xff000000),
        appBar: AppBar(
            backgroundColor: Colors.transparent,
            foregroundColor: Colors.limeAccent.shade400,
            title: Text("Users"),
            elevation: 0),
        body: Container(
            margin: EdgeInsets.only(left: 15, right: 15, top: 40, bottom: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  "CONNECTION ERROR",
                  style: TextStyle(
                      color: Colors.limeAccent.shade400,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                  textAlign: TextAlign.center,
                ),
                const Spacer(),
                const Image(
                  image: AssetImage("assets/icons/error.png"),
                  width: 120,
                  height: 120,
                ),
                const Spacer(),
                Text(
                  "THERE SEEMS TO BE AN ERROR WITH YOUR NETWORK ",
                  style: TextStyle(color: Colors.white),
                ),
                const Spacer(),
                Text(
                  ": /",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                const Spacer(),
                Text(
                  "TRY AGAIN LATER",
                  style: TextStyle(color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                const Spacer()
              ],
            )));
    */
  }
}

class VenuesPaginatedList extends StatefulWidget {
  VenuesPaginatedList({Key? key, required this.vm}) : super(key: key);
  ViewModel vm;

  @override
  State<StatefulWidget> createState() => _VenuesPaginatedState();
}

class _VenuesPaginatedState extends State<VenuesPaginatedList> {
  late ViewModel vm;
  String _userFavSport = "";
  Future<Null> getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      _userFavSport = prefs.getString("userSport")!;
    });
  }

  @override
  void initState() {
    super.initState();
    getSharedPrefs();
    vm = widget.vm;
  }

  @override
  Widget build(BuildContext context) {
    if (vm.loading) {
      return Expanded(
          child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [CircularProgressIndicator()])));
    }
    if (vm.error) {
      return Expanded(
          child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
            Text("NO MORE USERS AVAILABLE",
                style: TextStyle(color: Colors.white))
          ])));
    }
    return Expanded(
        child: ListView.separated(
            itemBuilder: (context, index) {
              Users users = vm.usersList[index];

              return (Container(
                  decoration: BoxDecoration(
                      color: Color(0xff2E3330),
                      borderRadius: BorderRadius.circular(15)),
                  padding: const EdgeInsets.all(10),
                  margin: const EdgeInsets.only(left: 15, right: 15),
                  //color: Color(0xff2E3330),

                  child: Container(
                      child: Row(
                    children: [
                      leftSection(users.pictureLocation),
                      middleSection(
                        users.name,
                        users.favSport == null
                            ? "no favourite sport"
                            : users.favSport,
                      ),
                      rightSection(users)
                    ],
                  ))));
            },
            separatorBuilder: (context, index) => Divider(),
            itemCount: vm.usersList.length));
  }

  leftSection(String url) {
    return Container(
      child: CachedNetworkImage(
        imageUrl: url,
        placeholder: (context, url) => const CircularProgressIndicator(),
        errorWidget: (context, url, error) => Text("no image"),
        imageBuilder: (context, imageProvider) => CircleAvatar(
          radius: 24,
          backgroundImage: imageProvider,
        ),
      ),
    );
  }

  middleSection(String name, String favSport) {
    return Expanded(
      child: Column(
        children: [
          Text(name,
              style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18)),
          Text(favSport,
              style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.normal,
                  fontSize: 10)),
          favSport == _userFavSport
              ? Icon(
                  Icons.check,
                  color: Colors.limeAccent.shade400,
                  size: 30.0,
                )
              : SizedBox.shrink(),
          Text(
            favSport == _userFavSport ? "Possible match" : "",
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 10),
          )
        ],
      ),
    );
  }

  rightSection(Users users) {
    return Material(
        color: const Color(0xff2E3330),
        child: IconButton(
          icon: Image.asset('assets/icons/triangle.png'),
          iconSize: 50,
          onPressed: () {
            /*
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => VenueDetail(venue: venue)),
            );
            */
          },
        ));
  }
}
