import 'package:athletim/bloc/bloc/auth_bloc.dart';
import 'package:athletim/screens/sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Widgets/friends_matches_widget.dart';
import '../Widgets/profile_pic_edit_widget.dart';

class ProfilePage extends StatefulWidget {
  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  late String _userInfo = "";

  Future<DocumentSnapshot<Object?>>? getId() async {
    CollectionReference users = FirebaseFirestore.instance.collection('users');
    final prefs = await SharedPreferences.getInstance();
    final String? userId = prefs.getString('userId');
    return users.doc(userId).get();
  }

  @override
  initState() {
    super.initState();
  }

  String value = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade900,
      appBar: AppBar(
          actions: <Widget>[
            PopupMenuButton<String>(
              onSelected: (value) {
                choiceAction(value, context);
              },
              itemBuilder: (BuildContext context) {
                return [
                  PopupMenuItem<String>(
                    value: "Sign out",
                    child: Text("Sign out"),
                  )
                ];
              },
            )
          ],
          backgroundColor: Colors.transparent,
          foregroundColor: Colors.limeAccent.shade400,
          elevation: 0),
      body: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is UnAuthenticated) {
            // Navigating to the dashboard screen if the user is authenticated
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => const SignIn()));
          }
          if (state is AuthError) {
            // Showing the error message if the user has entered invalid credentials
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(state.error)));
          }
        },
        child: BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            if (state is Loading) {
              // Showing the loading indicator while the user is signing in
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is Authenticated) {
              // Showing the sign in form if the user is not authenticated
              return FutureBuilder<DocumentSnapshot>(
                future: getId(),
                builder: (BuildContext context,
                    AsyncSnapshot<DocumentSnapshot> snapshot) {
                  if (snapshot.hasError) {
                    return CircularProgressIndicator();
                  }

                  if (snapshot.hasData && !snapshot.data!.exists) {
                    return Text("Document does not exist");
                  }

                  if (snapshot.connectionState == ConnectionState.done) {
                    Map<String, dynamic> data =
                        snapshot.data!.data() as Map<String, dynamic>;
                    print(data);
                    return ListView(
                      physics: BouncingScrollPhysics(),
                      children: [
                        Center(
                            child: ProfilePictureEditButton(
                          value: " ${data['pictureLocation']}",
                        )),
                        const SizedBox(height: 12),
                        Center(
                            child: Text(" ${data['name']}",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 30,
                                    color: Colors.white))),
                        Center(
                            child: Text(" ${data['username']}",
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 18,
                                    color: Colors.white))),
                        const SizedBox(
                          height: 14,
                        ),
                        FriendsGames(
                            value: " ${data['cantFriends']}",
                            value2: " ${data[' cantMatchesFavSport']}"),
                        const SizedBox(
                          height: 14,
                        ),
                        Center(
                            child: Text("Most played game",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white))),
                        const SizedBox(
                          height: 14,
                        ),
                        Center(
                            child: Image.asset(
                          "./assets/sports/" +
                              "${data['favSport'].toString().toLowerCase()}" +
                              ".png",
                          width: 70,
                        )),
                        const SizedBox(
                          height: 12,
                        ),
                        Center(
                            child: Text(
                                " ${data[' cantMatches']} matches played",
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12,
                                    color: Colors.white))),
                        const SizedBox(
                          height: 14,
                        ),
                        Center(
                            child: Image.asset(
                          "./assets/icons/stats.png",
                          width: 100,
                          color: Colors.limeAccent.shade400,
                        )),
                        Center(
                            child: Text("See your stats",
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12,
                                    color: Colors.white))),
                      ],
                    );
                  }

                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                  ;
                },
              );
              ;
            }
            return Container();
          },
        ),
      ),
    );
  }

  void choiceAction(String choice, BuildContext context) {
    if (choice == "Sign out") {
      _logOut(context);
    }
  }

  void _logOut(context) {
    BlocProvider.of<AuthBloc>(context).add(SignOutRequested());
  }
}
