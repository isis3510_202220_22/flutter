import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class ScheduleScreen extends StatefulWidget {

  const ScheduleScreen({Key? key}) : super(key: key);

  @override
  _ScheduleScreenState createState() => _ScheduleScreenState();
}

class _ScheduleScreenState extends State<ScheduleScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Container(
        child: SfCalendar(
          cellBorderColor: Colors.limeAccent.shade400,
          backgroundColor: Colors.grey.shade900,
          todayHighlightColor: Colors.limeAccent.shade400,
          headerStyle:
              CalendarHeaderStyle(textStyle: TextStyle(color: Colors.white)),
          timeSlotViewSettings: TimeSlotViewSettings(
            timelineAppointmentHeight: 40,
            timeTextStyle: TextStyle(color: Colors.white),
          ),
          viewHeaderStyle: ViewHeaderStyle(
              dayTextStyle: TextStyle(color: Colors.white),
              dateTextStyle: TextStyle(color: Colors.white)),
          todayTextStyle: TextStyle(
              fontStyle: FontStyle.italic, fontSize: 17, color: Colors.white),
        ),
      ),
    ));
  }
}
