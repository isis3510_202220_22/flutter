import 'package:athletim/Model/venue.dart';
import 'package:athletim/ViewModel/view_model.dart';
import 'package:athletim/bloc/bloc/auth_bloc.dart';
import 'package:athletim/screens/venue_detail_screen.dart';
import 'package:flutter/services.dart';
import 'package:getwidget/getwidget.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:location/location.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

class VenueScreen extends StatefulWidget {
  const VenueScreen({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<VenueScreen> {
  @override
  Widget build(BuildContext context) {
    return Home();
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

@override
void initState() {}

class _HomeState extends State<Home> {
  ConnectivityResult _connectionStatus = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;
  int pagingIndex = 1;

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      return;
    }
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }
    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      _connectionStatus = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    ViewModel viewModel = context.watch<ViewModel>();

    return _connectionStatus == ConnectivityResult.none
        ? noConnection()
        : Scaffold(
            backgroundColor: Color(0xff000000),
            appBar: AppBar(
                backgroundColor: Colors.transparent,
                foregroundColor: Colors.limeAccent.shade400,
                title: Text("Venues"),
                elevation: 0),
            body: Container(
              margin: EdgeInsets.only(bottom: 15),
              child: Column(children: [
                const Align(
                    alignment: Alignment.bottomLeft,
                    child: Text("\t\t Nearest venues:",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 25))),
                VenuesPaginatedList(key: UniqueKey(), vm: viewModel),
                Container(
                  margin: const EdgeInsets.only(left: 14, right: 14),
                  padding: const EdgeInsets.only(top: 1, bottom: 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Material(
                          color: Colors.transparent,
                          child: IconButton(
                            icon: Image.asset("assets/icons/leftArrow.png"),
                            onPressed: () => setState(() {
                              if (pagingIndex > 1) {
                                pagingIndex = pagingIndex - 1;
                                viewModel.updateList((pagingIndex - 1) * 5);
                              }
                            }),
                          )),
                      Text(
                        pagingIndex.toString(),
                        style: TextStyle(
                            color: Colors.limeAccent.shade400,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                      Material(
                          color: Colors.transparent,
                          child: IconButton(
                            icon: Image.asset("assets/icons/rightArrow.png"),
                            onPressed: () => setState(() {
                              pagingIndex = pagingIndex + 1;
                              viewModel.updateList((pagingIndex - 1) * 5);
                            }),
                          )),
                    ],
                  ),
                )
              ]),
            ));
  }

  noConnection() {
    return Scaffold(
        backgroundColor: Color(0xff000000),
        appBar: AppBar(
            backgroundColor: Colors.transparent,
            foregroundColor: Colors.limeAccent.shade400,
            title: Text("Venues"),
            elevation: 0),
        body: Container(
            margin: EdgeInsets.only(left: 15, right: 15, top: 40, bottom: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  "CONNECTION ERROR",
                  style: TextStyle(
                      color: Colors.limeAccent.shade400,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                  textAlign: TextAlign.center,
                ),
                const Spacer(),
                const Image(
                  image: AssetImage("assets/icons/error.png"),
                  width: 120,
                  height: 120,
                ),
                const Spacer(),
                Text(
                  "THERE SEEMS TO BE AN ERROR WITH YOUR NETWORK ",
                  style: TextStyle(color: Colors.white),
                ),
                const Spacer(),
                Text(
                  ": /",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                const Spacer(),
                Text(
                  "TRY AGAIN LATER",
                  style: TextStyle(color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                const Spacer()
              ],
            )));
  }
}

class VenuesPaginatedList extends StatefulWidget {
  VenuesPaginatedList({Key? key, required this.vm}) : super(key: key);
  ViewModel vm;

  @override
  State<StatefulWidget> createState() => _VenuesPaginatedState();
}

class _VenuesPaginatedState extends State<VenuesPaginatedList> {
  late ViewModel vm;
  Location location = Location();
  var latitudeInit = 0.0;
  var longitudeInit = 0.0;
  String _sport = "";
  late LocationData _currentPosition;

  Future<Null> getUserSport() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _sport = prefs.getString('userSport').toString();
    });
  }

  @override
  void initState() {
    super.initState();
    getUserSport();
    vm = widget.vm;
  }

  @override
  Widget build(BuildContext context) {
    getLoc();
    if (vm.loading) {
      return Expanded(
          child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [ CircularProgressIndicator()])));
    }
    if (vm.error) {
      return Expanded(
          child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
            Text("NO MORE VENUES AVAILABLE",
                style: TextStyle(color: Colors.white))
          ])));
    }
    return Expanded(
        child: ListView.separated(
            itemBuilder: (context, index) {
              vm.sortList(latitudeInit, longitudeInit);
              Venue venue = vm.venuesList[index];
              return (Container(
                  decoration: BoxDecoration(
                      color: Color(0xff2E3330),
                      borderRadius: BorderRadius.circular(15)),
                  padding: const EdgeInsets.all(10),
                  margin: const EdgeInsets.only(left: 15, right: 15),
                  //color: Color(0xff2E3330),

                  child: Container(
                      child: Row(
                    children: [
                      leftSection(venue.img),
                      middleSection(
                          venue.name,
                          vm
                                  .calcDif(venue, latitudeInit, longitudeInit)
                                  .toStringAsFixed(3) +
                              " km",
                          venue.sport,
                          _sport),
                      rightSection(venue)
                    ],
                  ))));
            },
            separatorBuilder: (context, index) => Divider(),
            itemCount: vm.venuesList.length));
  }

  leftSection(String url) {
    return Container(
      child: CachedNetworkImage(
        imageUrl: url,
        placeholder: (context, url) => const CircularProgressIndicator(),
        errorWidget: (context, url, error) =>
            Text("Not Able to retrieve image"),
        imageBuilder: (context, imageProvider) => CircleAvatar(
          radius: 24,
          backgroundImage: imageProvider,
        ),
      ),
    );
  }

  middleSection(
      String name, String distance, String venueSport, String userSport) {
    return venueSport.toLowerCase() == userSport.toLowerCase()
        ? Expanded(
            child: Column(
              children: [
                Row(mainAxisAlignment:MainAxisAlignment.center,children: [
                  Text(name + "\t",
                      style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 15)),
                  Icon( Icons.favorite, color: Colors.limeAccent.shade400, size: 20,),
                ]
                ),
                Text(distance,
                    style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.normal,
                        fontSize: 10))
              ],
            ),
          )
        : Expanded(
            child: Column(
              children: [
                Text(name,
                    style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18)),
                Text(distance,
                    style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.normal,
                        fontSize: 10))
              ],
            ),
          );
  }

  rightSection(Venue venue) {
    return Material(
        color: const Color(0xff2E3330),
        child: IconButton(
          icon: Image.asset('assets/icons/triangle.png'),
          iconSize: 50,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => VenueDetail(venue: venue)),
            );
          },
        ));
  }

  getLoc() async {
    final prefs = await SharedPreferences.getInstance();
    double? storedLatitude = await prefs.getDouble("latitude");
    double? storedLongitude = await prefs.getDouble("longitude");

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      if (storedLatitude != null && storedLongitude != null) {
        latitudeInit = storedLatitude;
        longitudeInit = storedLongitude;
        return;
      }
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    _currentPosition = await location.getLocation();
    if (latitudeInit != _currentPosition.latitude) {
      latitudeInit = _currentPosition.latitude as double;
      prefs.setDouble("latitude", latitudeInit);
    }
    if (longitudeInit != _currentPosition.longitude) {
      longitudeInit = _currentPosition.longitude as double;
      prefs.setDouble("longitude", longitudeInit);
    }
  }
}
