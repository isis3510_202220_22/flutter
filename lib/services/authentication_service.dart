import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthenticationService {
  final _firebaseAuth = FirebaseAuth.instance;
  Future<void> signUp({required String email, required String password}) async {
    final prefs = await SharedPreferences.getInstance();
    try {
      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      await prefs.setString('email', email);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        throw Exception('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        throw Exception('The account already exists for that email.');
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<void> register(
      {required String email,
      required String password,
      required String name,
      required String username,
      required String sport}) async {
    final _firebaseAuth = FirebaseAuth.instance;
    final prefs = await SharedPreferences.getInstance();
    try {
      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) async {
        FirebaseFirestore.instance
            .collection('users')
            .doc(value.user?.uid)
            .set({
          "email": email,
          "name": name,
          "username": username,
          "favSport": sport,
          "cantFriends": 0,
          "cantMatches": 0,
          "cantMatchesFavSport": 0
        });
        if (value.user?.uid != null) {
          await prefs.setString('userId', value.user!.uid);
          await prefs.setString('userSport', sport);
        }
      });
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        throw Exception('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        throw Exception('The account already exists for that email.');
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<void> signIn({
    required String email,
    required String password,
  }) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password)
          .then((value) async {
        if (value.user?.uid != null) {
          await prefs.setString('userId', value.user!.uid);
          var signedUser = await FirebaseFirestore.instance
              .collection('users')
              .doc(value.user!.uid)
              .get();

          prefs.setString('userSport', signedUser.get("favSport"));
        }
      });
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        throw Exception('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        throw Exception('Wrong password provided for that user.');
      }
    }
  }

  Future<void> signOut() async {
    try {
      await _firebaseAuth.signOut();
    } catch (e) {
      throw Exception(e);
    }
  }
}
